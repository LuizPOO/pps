package upisPPS;

public class Triangulo implements IFiguraBiDimensional {
	
	private int ladoAB;
	private int ladoBC;
	private int ladoCA;
	
	private boolean condicaoDeExistencia (int a, int b, int c) {
		
		return Math.abs (b - c) < a && a < b + c;
		
	}
	
	public Triangulo () {
		
		this.ladoAB = 1;
		this.ladoBC = 1;
		this.ladoCA = 1;
		
	}
	
	public Triangulo (int a, int b, int c) {
		
		if (!condicaoDeExistencia (a, b, c)) {
			throw new RuntimeException ("Imposs�vel construir tri�ngulo");
		}
		
			this.ladoAB = a;
			this.ladoBC = b;
			this.ladoCA = c;
			
		}
		
	@Override
	public String toString() {
		
		return "( " + this.ladoAB + ", " + this.ladoBC + ", " + this.ladoCA + " )";
		
	}

	public int getLadoAB() {
		
		return ladoAB;
		
	}
	
	public void setLadoAB(int a) {
		
		if (condicaoDeExistencia (a, ladoBC, ladoCA)) {
		
		this.ladoAB = a;
		
		}
	}
	
	public void setLadoAB(int a, int b) {
			
		if (condicaoDeExistencia (a, b, ladoCA)) {
			
		this.ladoAB = a;
		this.ladoBC = b;
			
		}
	}
	
	public void setLadoAB(int a, int b, int c) {
		
		if (condicaoDeExistencia (a, b, c)) {
			
		this.ladoAB = a;
		this.ladoBC = b;
		this.ladoCA = c;
			
		}
	}
	
	
	public int getLadoBC() {
		
		return ladoBC;
		
	}
	
	public void setLadoBC(int b) {
		
		if (condicaoDeExistencia (ladoAB, b, ladoCA)) {
		
		this.ladoBC = b;
		
		}
	}
	
    public void setLadoBC(int b, int c) {
		
		if (condicaoDeExistencia (ladoAB, b, c)) {
		
		this.ladoBC = b;
		this.ladoCA = c;
		
		}
    }
	
	public int getLadoCA() {
		
		return ladoCA;
		
	}
	
	public void setLadoCA(int c) {
		
		if (condicaoDeExistencia (ladoAB, ladoBC, c)) {
		
		this.ladoCA = c;
		
		}
	}
	
    public void setLadoCA(int c, int a) {
		
		if (condicaoDeExistencia (a, ladoBC, c)) {
		
		this.ladoCA = c;
		this.ladoAB = a;
		
		}
	}
		
	public double area () {
		
		int s = perimetro () / 2;
		int sa = s * (s - ladoAB) * (s - ladoBC) * (s - ladoCA);
		return Math.sqrt(sa);
		
	}
	
	public int perimetro () {
		
		return ladoAB + ladoBC + ladoCA;
		
	}
}
