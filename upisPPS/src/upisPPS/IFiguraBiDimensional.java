package upisPPS;

public interface IFiguraBiDimensional {
	
	public int perimetro();
	
	public double area();

}
