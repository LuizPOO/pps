package upisPPS;

public class Circulo implements IFiguraBiDimensional{
	
    private int raio;
	
	private boolean condicaoDeExistencia(int r) {
		
		return (r > 0);
		
	}
	
    public Circulo () {
		
		this.raio = 1;
		
    }
	
	public Circulo(int raio) {
		
		if (!condicaoDeExistencia(raio)) {
			
			throw new RuntimeException ("Impossível construir circulo");
			
		}
		
		this.raio = raio;
		
	}
	
	public int perimetro() {
		
		return  (int) (2 * 3.14 * raio);
		
	}

	public double area() {
		
		return 3.14 * (raio * raio);
		
	}
	
	@Override
    public String toString() {
		
		return "( " + this.raio + " )";
		
    }

	
    public int getRaio() {
    	
		return raio;
		
	}
    

	public void setRaio(int raio) {
		
		if (condicaoDeExistencia (raio)) {
	
		this.raio = raio;
		
		}
	}
   

}
