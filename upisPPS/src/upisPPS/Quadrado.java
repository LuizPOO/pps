package upisPPS;

public class Quadrado implements IFiguraBiDimensional{
	
	private int lado;
	
	private boolean condicaoDeExistencia(int l) {
		
		return (l > 0);
		
	}
	
    public Quadrado () {
		
		this.lado = 1;
		
    }
	
	public Quadrado(int lado) {
		
		if (!condicaoDeExistencia(lado)) {
			
			throw new RuntimeException ("Impossível construir quadrado");
			
		}
		
		this.lado = lado;
		
	}
	
	public int perimetro() {
		
		return 4 * lado;
		
	}

	public double area() {
		
		return lado * lado;
		
	}
	
    @Override
	public String toString() {
		
		return "( " + this.lado + "," + this.lado + "," + this.lado + "," + this.lado + " )";
		
    }

	
    public int getLado() {
    	
		return lado;
		
	}
    

	public void setLado(int lado) {
		
		if (condicaoDeExistencia (lado)) {
	
		this.lado = lado;
		
		}
	}
   
}
