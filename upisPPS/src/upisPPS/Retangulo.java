package upisPPS;

public class Retangulo implements IFiguraBiDimensional{
	
	private int comprimento;
	private int altura;
	
	private boolean condicaoDeExistencia(int c, int a) {
		
		return c > 0 && a > 0;
		
	}
	
    public Retangulo () {
		
		this.comprimento = 2;
		this.altura = 1;
		
    }
	
	public Retangulo(int comprimento, int altura) {
		
		if (!condicaoDeExistencia(comprimento, altura)) {
			
			throw new RuntimeException ("Impossível construir retangulo");
			
		}
		
		this.comprimento = comprimento;
		this.altura = altura;
		
	}
	
	public int perimetro() {
		
		return comprimento + comprimento + altura + altura;
		
	}

	public double area() {
		
		return comprimento * altura;
		
	}
	
    @Override
	public String toString() {
		
		return "( " + this.comprimento + ", " + this.comprimento + ", " + this.altura + ", " + this.altura + " )";
		
    }

	
    public int getComprimento() {
    	
		return comprimento;
		
	}
    

	public void setComprimento(int c) {
		
		if (condicaoDeExistencia (c, altura)) {
	
		this.comprimento = c;
		
		}
	}
	
    public void setComprimento(int c, int a) {
		
		if (condicaoDeExistencia (c, a)) {
	
		this.comprimento = c;
		this.altura = a;
		
		}
	}
	
    public int getAltura() {
    	
		return altura;
		
	}
    

	public void setAltura(int a) {
		
		if (condicaoDeExistencia (comprimento, a)) {
	
		this.altura = a;
		
		}
	}
}
