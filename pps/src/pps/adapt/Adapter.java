package pps.adapt;

import javax.swing.JOptionPane;

public class Adapter implements Target {
	
	private Adaptee adaptee;
	
	public Adapter() {
		adaptee = new Adaptee();
	}
	
	@Override
	public void exibirMensagem(String mssg, int tipo) {
		String str[] = mssg.split(":");
		
		if(tipo==JOptionPane.ERROR_MESSAGE) {
			str[0]=("ERR");
		}

		if(tipo==JOptionPane.WARNING_MESSAGE) {
			str[0]=("WARN");
		}

		if(tipo==JOptionPane.INFORMATION_MESSAGE) {
			str[0]=("INF");
		}

		String msg = str.length > 1 ? str[1] : str[0];
		
		adaptee.enviarMensagem(msg);
	}

}
