package pps.decorator;

public class Sanduiche implements ISanduiche {

	@Override
	public double valor() {
		return 1.2;
	}

	@Override
	public String descricao() {
		return "P�o";
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
}

