package pps.decorator.atividade;

public class Drink implements IDrink {

	@Override
	public double valor() {
		return 5.0;
	}

	@Override
	public String descricao() {
		return "Rum";
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
	
}
