package pps.decorator.atividade;

public class SucoDeAbacaxi extends Ingredientes{

	public SucoDeAbacaxi(IDrink pinaColada){
		super(pinaColada);
	}
	
	@Override
	public String descricao() {
		return pinaColada.descricao() + ", " + "Suco de Abacaxi";
	}
	
	@Override
	public double valor() {
		return pinaColada.valor() + 1;
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
	
}
