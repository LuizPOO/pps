package pps.decorator.atividade;

public class LeiteDeCoco extends Ingredientes{

	public LeiteDeCoco(IDrink pinaColada){
		super(pinaColada);
	}
	
	@Override
	public String descricao() {
		return pinaColada.descricao() + ", " + "Leite de Coco";
	}
	
	@Override
	public double valor() {
		return pinaColada.valor() + .5;
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
	
}
