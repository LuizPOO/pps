package pps.decorator.atividade;

public abstract class Ingredientes implements IDrink{

	IDrink pinaColada;
	
	public Ingredientes(IDrink pinaColada) {
		this.pinaColada = pinaColada;
	}
	
	@Override
	public double valor() {
		return pinaColada.valor();
	}
	
	public abstract String descricao();
	
}
