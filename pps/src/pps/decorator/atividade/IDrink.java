package pps.decorator.atividade;

public interface IDrink {

	public double valor();
	
	public String descricao();
	
}
