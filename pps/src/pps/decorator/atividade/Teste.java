package pps.decorator.atividade;

public class Teste {

	public static void main(String[] args) {
		
		IDrink d = new Drink();
		
		System.out.println(d);
		
		Ingredientes c = new LeiteCondensado(d);
		
		System.out.println(c);
		
		Ingredientes c1 = new LeiteDeCoco(c);
		
		System.out.println(c1);			
		
		Ingredientes c2 = new SucoDeAbacaxi(c1);
		
		System.out.println(c2);

        Ingredientes c3 = new Gelo(c2);

		System.out.println(c3);
		
	}
	
}
