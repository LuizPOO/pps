package pps.decorator.atividade;

public class LeiteCondensado extends Ingredientes{

	public LeiteCondensado (IDrink pinaColada) {
		super(pinaColada);
	}

	@Override
	public String descricao() {
		return pinaColada.descricao() + ", " + "Leite Condensado";
	}
	
	@Override
	public double valor() {
		return pinaColada.valor() + 1;
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
	
}
