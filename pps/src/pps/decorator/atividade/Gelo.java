package pps.decorator.atividade;

public class Gelo extends Ingredientes{

	public Gelo(IDrink pinaColada){
		super(pinaColada);
	}
	
	@Override
	public String descricao() {
		return pinaColada.descricao() + ", " + "Gelo";
	}
	
	@Override
	public double valor() {
		return pinaColada.valor() + 0.1;
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
	
}
