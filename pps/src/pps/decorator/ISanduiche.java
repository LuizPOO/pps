package pps.decorator;

public interface ISanduiche {

	public double valor();
	
	public String descricao();
}

