package pps.decorator;

public class Queijo extends Ingredientes{

	public Queijo(ISanduiche sanduba){
		super(sanduba);
	}
	
	@Override
	public String descricao() {
		return sanduba.descricao() + ", " + "queijo";
	}
	
	@Override
	public double valor() {
		return sanduba.valor() + .5;
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
}

