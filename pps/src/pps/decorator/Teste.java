package pps.decorator;

public class Teste {

	public static void main(String[] args) {
		
		ISanduiche s = new Sanduiche();
		
		System.out.println(s);
		
		Ingredientes c = new Carne(s);
		
		System.out.println(c);
		
		Ingredientes c1 = new Carne(c);
		
		System.out.println(c1);			
		
		Ingredientes q = new Queijo(c1);
		
		System.out.println(q);
		
	}
}

