package pps.decorator;

public abstract class Ingredientes implements ISanduiche{

	ISanduiche sanduba;
	
	public Ingredientes(ISanduiche sanduba) {
		this.sanduba = sanduba;
	}
	
	@Override
	public double valor() {
		return sanduba.valor();
	}
	
	public abstract String descricao();
}

