package pps.decorator;

public class Carne extends Ingredientes{

	public Carne(ISanduiche sanduba) {
		super(sanduba);
	}

	@Override
	public String descricao() {
		return sanduba.descricao() + ", " + "carne";
	}
	
	@Override
	public double valor() {
		return sanduba.valor() + 1;
	}
	
	@Override
	public String toString() {
		return "R$ " + valor() + ": " + descricao();
	}
}
