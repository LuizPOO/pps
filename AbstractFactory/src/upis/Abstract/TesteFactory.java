package upis.Abstract;

public class TesteFactory {
	
	private static Carro montarCarro(String modelo) {
		
		CarroFactory cf = null;
		
		if (modelo.equals("luxo")) {
			cf = new CarroLuxoFactory();
			
		}
		
		if (modelo.equals("popular")) {
			cf = new CarroPopularFactory();
			
		}
		Carro carro = new Carro();
		carro.setRoda(cf.montarRoda());
		carro.setSom(cf.montarSom());
		return carro;
	}

	public static void main (String[] args) {
		Carro carro1 = montarCarro("luxo");
		Carro carro2 = montarCarro("popular");
		
		System.out.println(carro1);
		System.out.println(carro2);
	}
}
