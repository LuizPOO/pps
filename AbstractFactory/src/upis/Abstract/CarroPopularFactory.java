package upis.Abstract;

public class CarroPopularFactory extends CarroFactory {
	/* Nossa outra f�brica concreta */

	@Override
	public Roda montarRoda() {
		return new RodaSimples();
	}

	@Override
	public Som montarSom() {
		return new TocaFitas();
	}

}
