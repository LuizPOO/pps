package upis.Abstract;

public class CarroLuxoFactory extends CarroFactory {
	/* Essa � nossa f�brica concreta, ela faz um extends a f�brica abstrata para especializar um carro */

	@Override
	public Roda montarRoda() {
		return new RodaLigaLeve();
		
	}

	@Override
	public Som montarSom() {
		return new CDPlayer();
		
	}
	

}
