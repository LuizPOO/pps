package upis.Abstract;

public abstract class CarroFactory {
	/* Essa � a classe mais gen�rica, por estar acima das outras, ela � abstrata para n�o se prender a muitos detalhes */
	
	public abstract Roda montarRoda();
	
	public abstract Som montarSom();

}
