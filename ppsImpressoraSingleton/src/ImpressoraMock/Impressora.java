package ImpressoraMock;

public class Impressora {
	
	private static Impressora instance;
	
	private Impressora() {
		
	}
	
	public static Impressora getinstance() throws InterruptedException {
		
		if (instance == null) {
			instance = new Impressora();
		}
		
		return instance;
	}
	
	public void imprimir(String arquivo, boolean colorido) {
		
		if (colorido) {
			
			System.out.println(arquivo);
			
		}else {
			
			System.out.println(arquivo);
			
		}
	}

}
