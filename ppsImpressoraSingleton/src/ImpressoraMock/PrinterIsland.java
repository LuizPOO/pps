package ImpressoraMock;

public class PrinterIsland {
	
	private static PrinterIsland instance[];
	
	private static int k;
	
	private static int MAX = 5;
	
	private PrinterIsland() {
		
	}
	
    public static PrinterIsland getinstance() {
		
		if (instance == null) {
			instance = new PrinterIsland[MAX];
			criarInstancias();
			
		}
		
		k = k + 1;
		if (k == MAX) {
			k = 0;
			
		}
		
		return instance[k];
		
	}
	
	public void imprimir(String arquivo, boolean colorido) {
		
		if (colorido) {
			
			System.out.println(arquivo);
			
		}else {
			
			System.out.println(arquivo);
			
		}
	}
	
	private static void criarInstancias() {
		
		for (int j = 0; j < MAX; j++) {
			instance[j] = new PrinterIsland();
		}
		
	}

}

