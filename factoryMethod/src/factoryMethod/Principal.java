package factoryMethod;

public class Principal {
	
	public static void main(String[] args) {
		
		IConstrucaoFabricaDeCarros fcar;
		
		fcar = new ConstrutorFabricaDeCarros();
		
		IFabricaDeCarros fc;
		
        fc = fcar.montar("hyundai");
		
		System.out.println(fc);
		
        fc = fcar.montar("chevrolet");
		
		System.out.println(fc);
		
        fc = fcar.montar("volkswagen");
		
		System.out.println(fc);
		
        fc = fcar.montar("peugeot");
		
		System.out.println(fc);
	}

}
