package factoryMethod;

public interface IConstrucaoFabricaDeCarros {
	
	public IFabricaDeCarros montar(String montadora);

}
