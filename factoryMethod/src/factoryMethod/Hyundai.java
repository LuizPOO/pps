package factoryMethod;

public class Hyundai implements IFabricaDeCarros {
	
	private String modelo;
	private String cor;
	private int ano;
	
	public Hyundai () {
		
		this.modelo = "HB20";
		this.cor = "Prata";
		this.ano = 2018;
		
	}
	
	public String modelo() {
		return this.modelo;
		
	}
	
	public String cor() {
		return this.cor;
		
	}
	
	public int ano() {
		return this.ano;
		
	}

	@Override
	public String toString() {
		return "Hyundai [modelo=" + modelo + ", cor=" + cor + ", ano=" + ano + "]";
		
	}

}
