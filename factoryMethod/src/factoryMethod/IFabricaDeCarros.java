package factoryMethod;

public interface IFabricaDeCarros {
	
	public String modelo();
	public String cor();
	public int ano();

}
