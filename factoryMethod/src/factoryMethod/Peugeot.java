package factoryMethod;

public class Peugeot implements IFabricaDeCarros {
	
	private String modelo;
	private String cor;
	private int ano;
	
	public Peugeot () {
		
		this.modelo = "208";
		this.cor = "Vermelho";
		this.ano = 2021;
		
	}
	
	public String modelo() {
		return this.modelo;
		
	}
	
	public String cor() {
		return this.cor;
		
	}
	
	public int ano() {
		return this.ano;
		
	}

	@Override
	public String toString() {
		return "Peugeot [modelo=" + modelo + ", cor=" + cor + ", ano=" + ano + "]";
		
	}

}
