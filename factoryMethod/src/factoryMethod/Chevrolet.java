package factoryMethod;

public class Chevrolet implements IFabricaDeCarros {

	private String modelo;
	private String cor;
	private int ano;
	
	public Chevrolet () {
		
		this.modelo = "Cobalt";
		this.cor = "Bege";
		this.ano = 2014;
		
	}
	
	public String modelo() {
		return this.modelo;
		
	}
	
	public String cor() {
		return this.cor;
		
	}
	
	public int ano() {
		return this.ano;
		
	}

	@Override
	public String toString() {
		return "Chevrolet [modelo=" + modelo + ", cor=" + cor + ", ano=" + ano + "]";
		
	}

}
