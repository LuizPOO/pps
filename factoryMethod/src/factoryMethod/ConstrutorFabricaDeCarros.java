package factoryMethod;

public class ConstrutorFabricaDeCarros implements IConstrucaoFabricaDeCarros {
	
	@Override
	public IFabricaDeCarros montar(String montadora) {
		
		if (montadora.equals("hyundai")) {
			return new Hyundai();
			
		}
		
		if (montadora.equals("chevrolet")) {
			return new Chevrolet();
			
		}
		
		if (montadora.equals("volkswagen")) {
			return new Volkswagen();
			
		}
		
		if (montadora.equals("peugeot")) {
			return new Peugeot();
			
		}
		
		return null;
		
	}

}
