package factoryMethod;

public class Volkswagen implements IFabricaDeCarros {

	private String modelo;
	private String cor;
	private int ano;
	
	public Volkswagen () {
		
		this.modelo = "Jetta";
		this.cor = "Branco";
		this.ano = 2017;
		
	}
	
	public String modelo() {
		return this.modelo;
		
	}
	
	public String cor() {
		return this.cor;
		
	}
	
	public int ano() {
		return this.ano;
		
	}

	@Override
	public String toString() {
		return "Volkswagen [modelo=" + modelo + ", cor=" + cor + ", ano=" + ano + "]";
		
	}

}
