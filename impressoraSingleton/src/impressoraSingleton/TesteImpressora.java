package impressoraSingleton;

public class TesteImpressora {
	
	public static void main(String[] args) {
		
		Impressora hp1 = Impressora.getINSTANCE();
		Impressora hp2 = Impressora.getINSTANCE();
		
		System.out.println(hp1);
		System.out.println(hp2);
		
	}

}
