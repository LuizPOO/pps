package impressoraSingleton;

public class Impressora {
	
	static private Impressora INSTANCE = new Impressora();
	
	private Impressora() {
		
	}
	
	static Impressora getINSTANCE() {
		
		if (INSTANCE == null) {
			INSTANCE = new Impressora();
		}
		
		return INSTANCE;
	}

}
